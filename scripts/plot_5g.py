import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib

lines = ["-", "--", "-.", ":", "-", "--", "-.", ":", "-", "--", "-.", ":", ]
linewidth = 2


def lost_messages_count(set, with_stops):
    df = pd.read_csv('../cmake-build-debug/tests/traffic/micro/Link_event_set' + str(set) + '.csv', sep=';')
    msg_count_cols = [col for col in df.columns if col.endswith(':messages_count')]
    lost_msg_count_cols = [col for col in df.columns if col.endswith(':lost_messages_count')]

    msg_count = 0
    lost_msg_count = 0
    for i in range(0, len(msg_count_cols)):
        msg_count += df[msg_count_cols[i]].iloc[-2]
        lost_msg_count += df[lost_msg_count_cols[i]].iloc[-2]
        i += 1

    print("total msg count ", msg_count)
    print("total lost msg count ", lost_msg_count)
    print("proba ", lost_msg_count / msg_count)


def average_message_delays_total(set):
    df = pd.read_csv('../cmake-build-debug/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    avg_delays = df.loc[df[':root:antenna_antenna/0:average_message_delays_total'] != -1][
        ':root:antenna_antenna/0:average_message_delays_total']
    times = df['time']
    times = times[avg_delays.first_valid_index():len(times) + 1]
    print(avg_delays)
    print(times)
    plt.plot(times, avg_delays * 1000)
    plt.xlabel('time(s)')
    plt.ylabel('avg delay(ms)')
    plt.xlim(times[times.first_valid_index()], times[times.last_valid_index() - 1])
    plt.tight_layout()
    plt.savefig('avg_delays_set' + str(set) + '.png')


def average_message_debits_total(set):
    df = pd.read_csv('../cmake-build-debug/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    avg_debits = df.loc[df[':root:antenna_antenna/0:average_message_debits_total'] != -1][
        ':root:antenna_antenna/0:average_message_debits_total']
    times = df['time']
    times = times[avg_debits.first_valid_index():len(times) + 1]
    print(avg_debits)
    print(times)
    plt.plot(times, avg_debits * 1e-6)
    plt.xlabel('time(s)')
    plt.ylabel('avg debit(Mbps)')
    plt.xlim(times[times.first_valid_index()], times[times.last_valid_index() - 1])
    plt.tight_layout()
    plt.savefig('avg_debits_set' + str(set) + '.png')


def average_vehicle_number(set, with_stops):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':vehicle_number')]
    avg = df[vehicle_cols].mean(axis=1)
    min = df[vehicle_cols].min(axis=1)
    max = df[vehicle_cols].max(axis=1)
    empty_links = df[df[vehicle_cols] == 0][vehicle_cols]
    # avg = df[vehicle_cols].max(axis=1)
    print(avg)
    plt.plot(df['time'], avg)
    plt.xlabel('time(s)')
    plt.ylabel('avg vehicle number')
    plt.savefig('avg_vehicle_number_set' + str(set) + '.png')
    plt.close()

    plt.plot(df['time'], min)
    plt.xlabel('time(s)')
    plt.ylabel('min vehicle number')
    plt.savefig('min_vehicle_number_set' + str(set) + '.png')
    plt.close()

    plt.plot(df['time'], max)
    plt.xlabel('time(s)')
    plt.ylabel('max vehicle number')
    plt.savefig('max_vehicle_number_set' + str(set) + '.png')
    plt.close()

    empty_count = []
    for i, idx in enumerate(df[vehicle_cols].index):
        sum = 0
        for col in df[vehicle_cols].loc[idx]:
            if col == 0:
                sum += 1
        empty_count.append(sum)
    print(empty_count)
    plt.plot(df['time'], empty_count)
    plt.xlabel('time(s)')
    plt.ylabel('empty links number')
    plt.tight_layout()
    plt.savefig('empty_links_number_set' + str(set) + '.png')
    plt.close()


def total_vehicle_number(set, with_stops):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':vehicle_number')]
    total = df[vehicle_cols].sum(axis=1)
    # avg = df[vehicle_cols].max(axis=1)
    print(total)
    plt.plot(df['time'], total)
    plt.xlabel('time(s)')
    plt.ylabel('total vehicle number')
    plt.tight_layout()
    plt.savefig('total_vehicle_number_set' + str(set) + '.png')


def cells_vehicle_count(set, frequencies):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    cells = df[":root:antenna_antenna/0:cells_vehicle_count"]
    times = df['time']
    print(cells.first_valid_index())
    times = times[cells.first_valid_index():len(times) - 1]
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    cells_ids = df[':root:antenna_antenna/0:cell_ids']
    cells_ids = cells_ids[cells_ids.first_valid_index():len(cells_ids) - 1].str.split(" ")
    list_cells = []
    list_counts = []

    for cell in cells_ids[0]:
        if cell != '':
            cell = cell.replace('antenna', 'Cell')
            list_cells.append(cell)
            list_counts.append([])

    for i, row_cell in enumerate(cells):
        for j, cell in enumerate(row_cell):
            if cell != '':
                list_counts[j].append(int(cell))

    for i, cell in enumerate(list_cells):
        show = False
        for f in frequencies:
            if f in cell:
                show = True
        if show:
            plt.plot(times, list_counts[i], label=cell, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('vehicles in cell')
    plt.legend()
    plt.tight_layout()
    plt.savefig('cells_vehicle_count_set' + str(set) + '_' + frequencies[0] + '.png')
    plt.close()

    list_sum = np.sum(list_counts, 0)
    plt.plot(times, list_sum, linestyle=lines[0], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('total vehicles in cells')
    plt.legend()
    plt.savefig('cells_total_vehicle_count_set' + str(set) + '_' + frequencies[0] + '.png')
    plt.close()


def total_transmitting_vehicles(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':transmitting_vehicles')]
    total = df[vehicle_cols].sum(axis=1)
    total = total[0:-1]
    # avg = df[vehicle_cols].max(axis=1)
    print(total)
    times = df['time'][0:-1]
    plt.plot(times, total)
    plt.xlabel('time(s)')
    plt.ylabel('total transmitting vehicle')
    plt.tight_layout()
    plt.savefig('total_transmitting_vehicle_set' + str(set) + '.png')


def report_per_vehicle(set):
    df = pd.read_csv('../cmake-build-debug/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    reports_df = df[":root:antenna_antenna/0:measure_reports_ID"]
    reports = reports_df.str.split(" ")
    vehicles = {}
    for report_row in reports:
        if not isinstance(report_row, float):
            for report in report_row:
                if report != '':
                    report = report.split("/")
                    vehicles[report[0]] = int(report[2])
    print(vehicles)
    print(len(vehicles))
    sum = 0
    for key in vehicles:
        sum += vehicles[key]
    print('sum', sum)
    print('average', sum / len(vehicles))


def average_message_debits_cell(set, use_distance, frequencies, use_cam):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    if use_distance:
        debits = df[":root:antenna_antenna/0:average_message_debits_distance"]
    else:
        if use_cam:
            debits = df[":root:antenna_antenna/0:average_cam_message_debits"]
        else:
            debits = df[":root:antenna_antenna/0:average_message_debits"]
    times = df['time']
    print(debits.first_valid_index())
    times = times[debits.first_valid_index():len(times) - 1]
    debits = debits[debits.first_valid_index():len(debits) - 1].str.split(" ")
    cells = df[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    list_debits = []
    list_counts = []

    for deb in cells[0]:
        if deb != '':
            if use_distance:
                # list_debits.append(deb.split(':')[0] + deb.split(':')[1])
                if '700' in deb:
                    list_debits.append(deb + ':o1200')
                    list_debits.append(deb + ':u1200')
                if '3500' in deb:
                    list_debits.append(deb + ':o150')
                    list_debits.append(deb + ':u150')
                list_counts.append([])
            else:
                # list_debits.append(deb.split(':')[0])
                deb = deb.replace('antenna', 'Cell')
                list_debits.append(deb)
            list_counts.append([])

    for i, row_deb in enumerate(debits):
        for j, deb in enumerate(row_deb):
            if deb != '':
                if use_distance:
                    # d = float(deb.split(":")[2])
                    d = float(deb)
                else:
                    # d = float(deb.split(":")[1])
                    d = float(deb)
                if d != 0:
                    list_counts[j].append(d * 1e-6)
                else:
                    list_counts[j].append(np.nan)
    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, deb in enumerate(list_debits):
        show = False
        for f in frequencies:
            if f in deb:
                if f == '3500' and 'u' in deb:
                    show = True
                if f == '700' and 'o' in deb:
                    show = True
                show = True
        if show:
            plt.plot(times, list_counts[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    if use_cam:
        plt.ylabel('CAM average speed(Mbps)')
    else:
        plt.ylabel('Measure Report average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    if use_cam:
        plt.savefig('average_cam_debits_cell_set' + str(set) + '_' + frequencies[0] + '.png')
    else:
        plt.savefig('average_debits_cell_set' + str(set) + '_' + frequencies[0] + '.png')


def average_message_delays_cell(set, use_distance, frequencies, use_cam, use_end_delay):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    if use_distance:
        delays = df[":root:antenna_antenna/0:average_message_delays_distance"]
    else:
        if use_cam:
            if use_end_delay:
                delays = df[":root:antenna_antenna/0:average_cam_message_end_delays"]
            else:
                delays = df[":root:antenna_antenna/0:average_cam_message_delays"]
        else:
            delays = df[":root:antenna_antenna/0:average_message_delays"]
    times = df['time']
    print(delays.first_valid_index())
    times = times[delays.first_valid_index():len(times) - 1]
    delays = delays[delays.first_valid_index():len(delays) - 1].str.split(" ")
    cells = df[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    list_delays = []
    list_counts = []

    for deb in cells[0]:
        if deb != '':
            if use_distance:
                # list_delays.append(deb.split(':')[0] + deb.split(':')[1])
                if '700' in deb:
                    list_delays.append(deb + ':o1200')
                    list_delays.append(deb + ':u1200')
                if '3500' in deb:
                    list_delays.append(deb + ':o150')
                    list_delays.append(deb + ':u150')
                list_counts.append([])
            else:
                # list_delays.append(deb.split(':')[0])
                deb = deb.replace('antenna', 'Cell')
                list_delays.append(deb)
            list_counts.append([])

    for i, row_deb in enumerate(delays):
        for j, deb in enumerate(row_deb):
            if deb != '':
                if use_distance:
                    # d = float(deb.split(":")[2])
                    d = float(deb)
                else:
                    # d = float(deb.split(":")[1])
                    d = float(deb)
                if d != 0:
                    list_counts[j].append(d * 1000)
                else:
                    list_counts[j].append(np.nan)
    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, deb in enumerate(list_delays):
        show = False
        for f in frequencies:
            if f in deb:
                show = True
        if show:
            plt.plot(times, list_counts[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    if use_cam:
        if use_end_delay:
            plt.ylabel('CAM average end delay(ms)')
        else:
            plt.ylabel('CAM average delay(ms)')
    else:
        plt.ylabel('Measure Report average delay(ms)')
    plt.legend()
    plt.tight_layout()
    if use_cam:
        if use_end_delay:
            plt.savefig('average_cam_end_delays_cell_set' + str(set) + '_' + frequencies[0] + '.png')
        else:
            plt.savefig('average_cam_delays_cell_set' + str(set) + '_' + frequencies[0] + '.png')
    else:
        plt.savefig('average_delays_cell_set' + str(set) + '_' + frequencies[0] + '.png')


def message_charges_cell(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    charges = df[":root:antenna_antenna/0:charge"]
    times = df['time']
    print(charges.first_valid_index())
    times = times[charges.first_valid_index():len(times) - 1]
    charges = charges[charges.first_valid_index():len(charges) - 1].str.split(" ")
    cells = df[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    list_charges = []
    list_counts = []

    for deb in cells[0]:
        if deb != '':
            list_charges.append(deb)
            list_counts.append([])

    for i, row_deb in enumerate(charges):
        for j, deb in enumerate(row_deb):
            if deb != '':
                d = float(deb)
                if d != 0:
                    list_counts[j].append(d)
                else:
                    list_counts[j].append(np.nan)
    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, deb in enumerate(list_charges):
        plt.plot(times, list_counts[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('charge (Mb)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('charges_cell_set' + str(set) + '.png')


def average_message_distance(set, frequencies, use_cam):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    if use_cam:
        cells = df[":root:antenna_antenna/0:average_cam_message_distance"]
    else:
        cells = df[":root:antenna_antenna/0:average_message_distance"]
    times = df['time']
    print(cells.first_valid_index())
    times = times[cells.first_valid_index():len(times) - 1]
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    cells_ids = df[':root:antenna_antenna/0:cell_ids']
    cells_ids = cells_ids[cells_ids.first_valid_index():len(cells_ids) - 1].str.split(" ")
    list_cells = []
    list_counts = []

    for cell in cells_ids[0]:
        if cell != '':
            cell = cell.replace('antenna', 'Cell')
            list_cells.append(cell)
            list_counts.append([])

    for i, row_cell in enumerate(cells):
        for j, cell in enumerate(row_cell):
            if cell != '':
                if cell != '0':
                    list_counts[j].append(float(cell))
                else:
                    list_counts[j].append(np.nan)
    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, cell in enumerate(list_cells):
        show = False
        for f in frequencies:
            if f in cell:
                show = True
        if show:
            plt.plot(times, list_counts[i], label=cell, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('average distance of vehicles in Cell(m)')
    plt.legend()
    plt.tight_layout()
    if use_cam:
        plt.savefig('avg_cam_distance_set' + str(set) + '_' + frequencies[0] + '.png')
    else:
        plt.savefig('avg_distance_set' + str(set) + '_' + frequencies[0] + '.png')
    plt.close()


def active_vehicles_cells(set, frequencies):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    cells = df[":root:antenna_antenna/0:active_vehicles_cells"]
    times = df['time']
    print(cells.first_valid_index())
    times = times[cells.first_valid_index():len(times) - 1]
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    cells_ids = df[':root:antenna_antenna/0:cell_ids']
    cells_ids = cells_ids[cells_ids.first_valid_index():len(cells_ids) - 1].str.split(" ")
    list_cells = []
    list_counts = []

    for cell in cells_ids[0]:
        if cell != '':
            cell = cell.replace('antenna', 'Cell')
            list_cells.append(cell)
            list_counts.append([])

    for i, row_cell in enumerate(cells):
        for j, cell in enumerate(row_cell):
            if cell != '':
                list_counts[j].append(float(cell))

    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, cell in enumerate(list_cells):
        show = False
        for f in frequencies:
            if f in cell:
                show = True
        if show:
            plt.plot(times, list_counts[i], label=cell, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('active vehicles')
    plt.legend()
    plt.tight_layout()
    plt.savefig('active_vehicles_set' + str(set) + '.png')


def transmit_vehicles_cells(set, frequencies):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv', sep=';')
    cells = df[":root:antenna_antenna/0:transmitting_vehicles_cells"]
    times = df['time']
    print(cells.first_valid_index())
    times = times[cells.first_valid_index():len(times) - 1]
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")
    cells_ids = df[':root:antenna_antenna/0:cell_ids']
    cells_ids = cells_ids[cells_ids.first_valid_index():len(cells_ids) - 1].str.split(" ")
    list_cells = []
    list_counts = []

    for cell in cells_ids[0]:
        if cell != '':
            cell = cell.replace('antenna', 'Cell')
            list_cells.append(cell)
            list_counts.append([])

    for i, row_cell in enumerate(cells):
        for j, cell in enumerate(row_cell):
            if cell != '':
                list_counts[j].append(float(cell))

    print(times)
    print(len(list_counts[0]), list_counts[0])

    for i, cell in enumerate(list_cells):
        show = False
        for f in frequencies:
            if f in cell:
                show = True
        if show:
            plt.plot(times, list_counts[i], label=cell, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('transmitting vehicles')
    plt.legend()
    plt.tight_layout()
    plt.savefig('transmitting_vehicles_set' + str(set) + '.png')


def average_denm_message_debits_downlink(set, use_distance, frequencies):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_set' + str(set) + '.csv', sep=';')
    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv',
                             sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':average_denm_debits')]

    used_cols_cells = []
    cells = df_antenna[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")

    avg_all_cols = []

    list_debits_all = []
    for deb in cells[0]:
        if deb != '':
            deb = deb.replace('antenna', 'Cell')
            list_debits_all.append(deb)
            avg_all_cols.append([])
            used_cols_cells.append([])

    for col in vehicle_cols:
        use_col_cells = [False, False, False, False, False, False]
        debits = df[col]
        times = df['time']
        times = times[debits.first_valid_index():len(times) - 1]
        debits = debits[debits.first_valid_index():len(debits) - 1].str.split(" ")

        list_debits = []
        list_counts = []

        for deb in cells[0]:
            if deb != '':
                if use_distance:
                    # list_debits.append(deb.split(':')[0] + deb.split(':')[1])
                    if '700' in deb:
                        list_debits.append(deb + ':o1200')
                        list_debits.append(deb + ':u1200')
                    if '3500' in deb:
                        list_debits.append(deb + ':o150')
                        list_debits.append(deb + ':u150')
                    list_counts.append([])
                else:
                    # list_debits.append(deb.split(':')[0])
                    list_debits.append(deb)
                list_counts.append([])

        for i, row_deb in enumerate(debits):
            for j, deb in enumerate(row_deb):
                if deb != '':
                    if use_distance:
                        # d = float(deb.split(":")[2])
                        d = float(deb)
                    else:
                        # d = float(deb.split(":")[1])
                        d = float(deb)
                    if d != 0:
                        list_counts[j].append(d * 1e-6)
                        use_col_cells[j] = True
                    else:
                        list_counts[j].append(0)

        print(list_counts)
        for i, deb in enumerate(list_debits):
            show = False
            for f in frequencies:
                if f in deb:
                    if f == '3500' and 'u' in deb:
                        show = True
                    if f == '700' and 'o' in deb:
                        show = True
                    show = True
            if show and use_col_cells[i]:
                if not np.any(avg_all_cols[i]):
                    avg_all_cols[i] = np.array(list_counts[i])
                else:
                    avg_all_cols[i] += np.array(list_counts[i])

        for i, use_col in enumerate(use_col_cells):
            if use_col:
                used_cols_cells[i].append(col)

    for i, deb in enumerate(list_debits):
        show = False
        for f in frequencies:
            if f in deb:
                if f == '3500' and 'u' in deb:
                    show = True
                if f == '700' and 'o' in deb:
                    show = True
                show = True
        if len(avg_all_cols[i]) == 0:
            show = False
        if show:
            avg_all_cols[i] /= len(used_cols_cells[i])
            avg_all_cols[i][avg_all_cols[i] == 0] = np.nan
            plt.plot(times, avg_all_cols[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('DENM average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_denm_debits_cell_set' + str(set) + '_' + frequencies[0] + '.png')


def average_denm_message_delays(set, use_distance, frequencies):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_set' + str(set) + '.csv', sep=';')
    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv',
                             sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':average_denm_delays')]

    used_cols_cells = []
    cells = df_antenna[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")

    avg_all_cols = []

    list_delays_all = []
    for deb in cells[0]:
        if deb != '':
            deb = deb.replace('antenna', 'Cell')
            list_delays_all.append(deb)
            avg_all_cols.append([])
            used_cols_cells.append([])

    for col in vehicle_cols:
        use_col_cells = [False, False, False, False, False, False]
        delays = df[col]
        times = df['time']
        times = times[delays.first_valid_index():len(times) - 1]
        delays = delays[delays.first_valid_index():len(delays) - 1].str.split(" ")

        list_delays = []
        list_counts = []

        for deb in cells[0]:
            if deb != '':
                deb = deb.replace('antenna', 'Cell')
                if use_distance:
                    # list_delays.append(deb.split(':')[0] + deb.split(':')[1])
                    if '700' in deb:
                        list_delays.append(deb + ':o1200')
                        list_delays.append(deb + ':u1200')
                    if '3500' in deb:
                        list_delays.append(deb + ':o150')
                        list_delays.append(deb + ':u150')
                    list_counts.append([])
                else:
                    # list_delays.append(deb.split(':')[0])
                    list_delays.append(deb)
                list_counts.append([])

        for i, row_deb in enumerate(delays):
            for j, deb in enumerate(row_deb):
                if deb != '':
                    if use_distance:
                        # d = float(deb.split(":")[2])
                        d = float(deb)
                    else:
                        # d = float(deb.split(":")[1])
                        d = float(deb)
                    if d != 0:
                        list_counts[j].append(d * 1000)
                        use_col_cells[j] = True
                    else:
                        list_counts[j].append(0)
                        use_col_cells[j] = True

        for i, deb in enumerate(list_delays):
            show = False
            for f in frequencies:
                if f in deb:
                    if f == '3500' and 'u' in deb:
                        show = True
                    if f == '700' and 'o' in deb:
                        show = True
                    show = True
            if show and use_col_cells[i]:
                if not np.any(avg_all_cols[i]):
                    avg_all_cols[i] = np.array(list_counts[i])
                else:
                    avg_all_cols[i] += np.array(list_counts[i])

        for i, use_col in enumerate(use_col_cells):
            if use_col:
                used_cols_cells[i].append(col)

    for i, deb in enumerate(list_delays):
        show = False
        for f in frequencies:
            if f in deb:
                if f == '3500' and 'u' in deb:
                    show = True
                if f == '700' and 'o' in deb:
                    show = True
                show = True
        if show:
            avg_all_cols[i] = avg_all_cols[i].astype('float')
            avg_all_cols[i] /= len(used_cols_cells[i])
            avg_all_cols[i][avg_all_cols[i] == 0] = np.nan
            plt.plot(times, avg_all_cols[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    plt.ylabel('DENM average delay(ms)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_denm_delays_cell_set' + str(set) + '_' + frequencies[0] + '.png')


def average_message_delays_relays(set, use_distance, frequencies, use_cam, use_end_delay):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_set' + str(set) + '.csv', sep=';')
    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_set' + str(set) + '.csv',
                             sep=';')

    if use_distance:
        col_name = ':average_message_delays_distance'
    else:
        if use_cam:
            if use_end_delay:
                col_name = ':average_cam_message_end_delays'
            else:
                col_name = ':average_cam_message_delays'
        else:
            col_name = ':average_message_delays'
    vehicle_cols = [col for col in df.columns if col.endswith(col_name)]

    used_cols_cells = []
    cells = df_antenna[':root:antenna_antenna/0:cell_ids']
    cells = cells[cells.first_valid_index():len(cells) - 1].str.split(" ")

    avg_all_cols = []

    list_delays_all = []
    for deb in cells[0]:
        if deb != '':
            deb = deb.replace('antenna', 'Cell')
            list_delays_all.append(deb)
            avg_all_cols.append([])
            used_cols_cells.append([])

    for col in vehicle_cols:
        use_col_cells = [False, False, False, False, False, False]
        delays = df[col]
        times = df['time']
        times = times[delays.first_valid_index():len(times) - 1]
        delays = delays[delays.first_valid_index():len(delays) - 1].str.split(" ")

        list_delays = []
        list_counts = []

        for deb in cells[0]:
            if deb != '':
                deb = deb.replace('antenna', 'Cell')
                if use_distance:
                    # list_delays.append(deb.split(':')[0] + deb.split(':')[1])
                    if '700' in deb:
                        list_delays.append(deb + ':o1200')
                        list_delays.append(deb + ':u1200')
                    if '3500' in deb:
                        list_delays.append(deb + ':o150')
                        list_delays.append(deb + ':u150')
                    list_counts.append([])
                else:
                    # list_delays.append(deb.split(':')[0])
                    list_delays.append(deb)
                list_counts.append([])

        for i, row_deb in enumerate(delays):
            for j, deb in enumerate(row_deb):
                if deb != '':
                    if use_distance:
                        # d = float(deb.split(":")[2])
                        d = float(deb)
                    else:
                        # d = float(deb.split(":")[1])
                        d = float(deb)
                    if d != 0:
                        list_counts[j].append(d * 1000)
                        use_col_cells[j] = True
                    else:
                        list_counts[j].append(0)

        for i, deb in enumerate(list_delays):
            show = False
            for f in frequencies:
                if f in deb:
                    if f == '3500' and 'u' in deb:
                        show = True
                    if f == '700' and 'o' in deb:
                        show = True
                    show = True
            if show and use_col_cells[i]:
                if not np.any(avg_all_cols[i]):
                    avg_all_cols[i] = np.array(list_counts[i])
                else:
                    avg_all_cols[i] += np.array(list_counts[i])

        for i, use_col in enumerate(use_col_cells):
            if use_col:
                used_cols_cells[i].append(col)

    for i, deb in enumerate(list_delays):
        show = False
        for f in frequencies:
            if f in deb:
                if f == '3500' and 'u' in deb:
                    show = True
                if f == '700' and 'o' in deb:
                    show = True
                show = True
        if show:
            avg_all_cols[i] /= len(used_cols_cells[i])
            avg_all_cols[i][avg_all_cols[i] == 0] = np.nan
            plt.plot(times, avg_all_cols[i], label=deb, linestyle=lines[i], linewidth=linewidth)
    plt.xlabel('time(s)')
    if use_cam:
        if use_end_delay:
            plt.ylabel('CAM average end delay(ms)')
        else:
            plt.ylabel('CAM average delay(ms)')
    else:
        plt.ylabel('Measure Report average delay(ms)')
    plt.legend()
    plt.tight_layout()
    if use_cam:
        if use_end_delay:
            plt.savefig('average_cam_end_delays_relays_cell_set' + str(set) + '_' + frequencies[0] + '.png')
        else:
            plt.savefig('average_cam_delays_relays_cell_set' + str(set) + '_' + frequencies[0] + '.png')
    else:
        plt.savefig('average_delays_relays_cell_set' + str(set) + '_' + frequencies[0] + '.png')


def average_serving_cell_changes_number(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':serving_cell_changes')]
    avg = df[vehicle_cols].mean(axis=1)
    avg = df[df[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    min = df[vehicle_cols].min(axis=1)
    max = df[vehicle_cols].max(axis=1)
    empty_links = df[df[vehicle_cols] == 0][vehicle_cols]
    # avg = df[vehicle_cols].max(axis=1)
    print(avg)
    plt.plot(df['time'], avg)
    plt.xlabel('time(s)')
    plt.ylabel('average serving cell changes')
    plt.tight_layout()
    plt.savefig('average_serving_cell_changes_set' + str(set) + '.png')
    plt.close()

    plt.plot(df['time'], max)
    plt.xlabel('time(s)')
    plt.ylabel('max serving cell changes')
    plt.tight_layout()
    plt.savefig('max_serving_cell_changes_set' + str(set) + '.png')
    plt.close()

    empty_count = []
    for i, idx in enumerate(df[vehicle_cols].index):
        sum = 0
        for col in df[vehicle_cols].loc[idx]:
            if col == 0:
                sum += 1
        empty_count.append(sum)
    print(empty_count)
    plt.plot(df['time'], empty_count)
    plt.xlabel('time(s)')
    plt.ylabel('empty serving cell changes number')
    plt.tight_layout()
    plt.savefig('empty_serving_cell_changes_set' + str(set) + '.png')
    plt.close()


def average_neighbor_cells_number(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':average_neighbor_cells_number')]
    avg = df[vehicle_cols].mean(axis=1)
    avg = df[df[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    min = df[vehicle_cols].min(axis=1)
    max = df[vehicle_cols].max(axis=1)
    empty_links = df[df[vehicle_cols] == 0][vehicle_cols]
    # avg = df[vehicle_cols].max(axis=1)
    print(avg)
    plt.plot(df['time'], avg)
    plt.xlabel('time(s)')
    plt.ylabel('average neighbor cells per vehicle')
    plt.tight_layout()
    plt.savefig('average_neighbor_cells_set' + str(set) + '.png')
    plt.close()

    plt.plot(df['time'], max)
    plt.xlabel('time(s)')
    plt.ylabel('max neighbor cells per vehicle')
    plt.tight_layout()
    plt.savefig('max_neighbor_cells_set' + str(set) + '.png')
    plt.close()

    empty_count = []
    for i, idx in enumerate(df[vehicle_cols].index):
        sum = 0
        for col in df[vehicle_cols].loc[idx]:
            if col == 0:
                sum += 1
        empty_count.append(sum)
    print(empty_count)
    plt.plot(df['time'], empty_count)
    plt.xlabel('time(s)')
    plt.ylabel('empty neighbor cells per vehicle')
    plt.tight_layout()
    plt.savefig('empty_neighbor_cells_set' + str(set) + '.png')
    plt.close()


def total_vehicle_number_denm_signal(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':denm_vehicle_count')]
    total = df[vehicle_cols].sum(axis=1)
    # avg = df[vehicle_cols].max(axis=1)
    print(total)
    plt.plot(df['time'], total)
    plt.xlabel('time(s)')
    plt.ylabel('total vehicle number')
    plt.tight_layout()
    plt.savefig('denm_total_vehicle_number_signal_set' + str(set) + '.png')


def total_vehicle_number_relay(set):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':vehicle_count')]
    total = df[vehicle_cols].sum(axis=1)
    # avg = df[vehicle_cols].max(axis=1)
    print(total)
    plt.plot(df['time'], total)
    plt.xlabel('time(s)')
    plt.ylabel('total vehicle number')
    plt.tight_layout()
    plt.savefig('relay_total_vehicle_number_set' + str(set) + '.png')


def total_vehicle_number_all(set, use_relay):
    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':vehicle_number')]
    total_mobility = df[vehicle_cols].sum(axis=1)

    df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Signal_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':denm_vehicle_count')]
    total_denm = df[vehicle_cols].sum(axis=1)

    if use_relay:
        df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    else:
        df = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df.columns if col.endswith(':vehicle_count')]
    total_relay = df[vehicle_cols].sum(axis=1)
    # avg = df[vehicle_cols].max(axis=1)
    print(total_relay)

    plt.plot(df['time'], total_denm, label='denm')
    plt.plot(df['time'], total_relay, label='relay')
    plt.plot(df['time'], total_mobility, label='mobility')
    plt.xlabel('time(s)')
    plt.ylabel('total vehicle number')
    plt.legend()
    plt.tight_layout()
    plt.savefig('total_vehicle_number_all_set' + str(set) + '.png')
    plt.close()

    plt.plot(df['time'], total_denm - total_mobility, label='denm - mobility')
    plt.plot(df['time'], total_relay - total_mobility, label='relay - mobility')
    plt.xlabel('time(s)')
    plt.ylabel('total vehicle number')
    plt.legend()
    plt.tight_layout()
    plt.savefig('total_vehicle_number_difference_set' + str(set) + '.png')
    plt.close()


def average_cam_debits_uplink_relay(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    name_relay = 'relay'
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_debits_nocell')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1e-6
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_cam_uplink_debit_relay_' + str(set) + '.png')
    plt.close()
    
    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    name_antenna = 'antenna'
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_debits_nocell')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1e-6
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_cam_uplink_debit_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_cam_uplink_debit_combined_' + str(set) + '.png')
    plt.close()


def average_cam_delays_uplink_relay(set, use_end_delay):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    if use_end_delay:
        vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_delays_nocell')]
    else:
        vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_delays_nocell')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1000
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    if use_end_delay:
        plt.ylabel('average end delay(ms)')
    else:
        plt.ylabel('average delay(ms)')
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_cam_uplink_end_delay_relay_' + str(set) + '.png')
    else:
        plt.savefig('average_cam_uplink_delay_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    if use_end_delay:
        vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_delays_nocell')]
    else:
        vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_delays_nocell')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1000
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    if use_end_delay:
        plt.ylabel('average end delay(ms)')
    else:
        plt.ylabel('average delay(ms)')
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_cam_uplink_end_delay_antenna_' + str(set) + '.png')
    else:
        plt.savefig('average_cam_uplink_delay_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.legend()
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_cam_uplink_end_delay_combined_' + str(set) + '.png')
    else:
        plt.savefig('average_cam_uplink_delay_combined_' + str(set) + '.png')
    plt.close()


def average_denm_debits_uplink_relay(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    name_relay = 'relay'
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_denm_message_debits_nocell')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1e-6
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_denm_uplink_debit_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    name_antenna = 'antenna'
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_denm_message_debits_nocell')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1e-6
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_denm_uplink_debit_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_denm_uplink_debit_combined_' + str(set) + '.png')
    plt.close()


def average_denm_delays_uplink_relay(set, use_end_delay):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    if use_end_delay:
        vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_denm_message_delays_nocell')]
    else:
        vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_denm_message_delays_nocell')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1000
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    if use_end_delay:
        plt.ylabel('average end delay(ms)')
    else:
        plt.ylabel('average delay(ms)')
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_denm_uplink_end_delay_relay_' + str(set) + '.png')
    else:
        plt.savefig('average_denm_uplink_delay_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    if use_end_delay:
        vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_denm_message_delays_nocell')]
    else:
        vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_denm_message_delays_nocell')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1000
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    if use_end_delay:
        plt.ylabel('average end delay(ms)')
    else:
        plt.ylabel('average delay(ms)')
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_denm_uplink_end_delay_antenna_' + str(set) + '.png')
    else:
        plt.savefig('average_denm_uplink_delay_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.legend()
    plt.tight_layout()
    if use_end_delay:
        plt.savefig('average_denm_uplink_end_delay_combined_' + str(set) + '.png')
    else:
        plt.savefig('average_denm_uplink_delay_combined_' + str(set) + '.png')
    plt.close()


def average_cam_debits_downlink_link(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_debits')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1e-6
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_cam_downlink_debit_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_antenna_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_debits')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1e-6
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_cam_downlink_debit_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_cam_downlink_debit_combined_' + str(set) + '.png')
    plt.close()


def average_cam_delays_downlink_link(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_delays')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1000
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.tight_layout()
    plt.savefig('average_cam_downlink_delay_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_antenna_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_delays')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1000
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.tight_layout()
    plt.savefig('average_cam_downlink_delay_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_cam_downlink_delay_combined_' + str(set) + '.png')
    plt.close()


def average_denm_debits_downlink_link(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_denm_message_debits')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1e-6
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_denm_downlink_debit_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_antenna_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_denm_message_debits')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1e-6
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.tight_layout()
    plt.savefig('average_denm_downlink_debit_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average speed(Mbps)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_denm_downlink_debit_combined_' + str(set) + '.png')
    plt.close()


def average_denm_delays_downlink_link(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_relay_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_denm_message_delays')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay * 1000
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.tight_layout()
    plt.savefig('average_denm_downlink_delay_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Link_event_antenna_set' + str(set) + '.csv', sep=';')
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_denm_message_delays')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna * 1000
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.tight_layout()
    plt.savefig('average_denm_downlink_delay_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average delay(ms)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_denm_downlink_delay_combined_' + str(set) + '.png')
    plt.close()


def average_cam_distances_uplink_relay(set):
    df_relay = pd.read_csv('../cmake-build-release/tests/traffic/micro/Relay_event_relay_set' + str(set) + '.csv', sep=';')
    name_relay = 'relay'
    vehicle_cols = [col for col in df_relay.columns if col.endswith(':average_cam_message_distances_nocell')]
    avg_relay = df_relay[vehicle_cols].mean(axis=1)
    avg_relay = df_relay[df_relay[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_relay = avg_relay
    min = df_relay[vehicle_cols].min(axis=1)
    max = df_relay[vehicle_cols].max(axis=1)
    empty_links = df_relay[df_relay[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_relay['time'], avg_relay)
    plt.xlabel('time(s)')
    plt.ylabel('average distance(m)')
    plt.tight_layout()
    plt.savefig('average_cam_uplink_distance_relay_' + str(set) + '.png')
    plt.close()

    df_antenna = pd.read_csv('../cmake-build-release/tests/traffic/micro/Antenna_event_antenna_set' + str(set) + '.csv', sep=';')
    name_antenna = 'antenna'
    vehicle_cols = [col for col in df_antenna.columns if col.endswith(':average_cam_message_distances_nocell')]
    avg_antenna = df_antenna[vehicle_cols].mean(axis=1)
    avg_antenna = df_antenna[df_antenna[vehicle_cols] != 0][vehicle_cols].mean(axis=1)
    avg_antenna = avg_antenna
    min = df_antenna[vehicle_cols].min(axis=1)
    max = df_antenna[vehicle_cols].max(axis=1)
    empty_links = df_antenna[df_antenna[vehicle_cols] == 0][vehicle_cols]
    plt.plot(df_antenna['time'], avg_antenna)
    plt.xlabel('time(s)')
    plt.ylabel('average distance(m)')
    plt.tight_layout()
    plt.savefig('average_cam_uplink_distance_antenna_' + str(set) + '.png')
    plt.close()

    plt.plot(df_antenna['time'], avg_relay, label='relays')
    plt.plot(df_antenna['time'], avg_antenna, label='antenna')
    plt.xlabel('time(s)')
    plt.ylabel('average distance(m)')
    plt.legend()
    plt.tight_layout()
    plt.savefig('average_cam_uplink_distance_combined_' + str(set) + '.png')
    plt.close()
    
def main():
    plt.rcParams.update({'font.size': 13})
    set = 6
    with_stops = False
    use_distance = False
    use_cam = True
    use_end_delay = True
    use_relay = False
    frequencies = ['3500']
    # average_vehicle_number(set, with_stops)
    # average_message_delays_total(set)
    # average_message_debits_total(set)
    # total_vehicle_number(set, with_stops)
    # cells_vehicle_count(set, frequencies)
    # total_transmitting_vehicles(set)
    # report_per_vehicle(set)
    # average_message_debits_cell(set, use_distance, frequencies, use_cam)
    # average_message_delays_cell(set, use_distance, frequencies, use_cam, use_end_delay)
    # message_charges_cell(set)
    # average_message_distance(set, frequencies, use_cam)
    # active_vehicles_cells(set, frequencies)
    # transmit_vehicles_cells(set, frequencies)
    # average_denm_message_debits_downlink(set, use_distance, frequencies)
    # average_denm_message_delays(set, use_distance, frequencies)
    #  average_message_delays_relays(set, use_distance, frequencies, use_cam, use_end_delay)
    # average_serving_cell_changes_number(set)
    # average_neighbor_cells_number(set)
    # total_vehicle_number_denm_signal(set)
    # total_vehicle_number_relay(set)
    # total_vehicle_number_all(set, use_relay)
    # average_cam_debits_uplink_relay(set)
    # average_cam_delays_uplink_relay(set, use_end_delay)
    # average_denm_debits_uplink_relay(set)
    # average_denm_delays_uplink_relay(set, use_end_delay)
    # average_cam_debits_downlink_link(set)
    # average_cam_delays_downlink_link(set)
    # average_denm_debits_downlink_link(set)
    # average_denm_delays_downlink_link(set)
    average_cam_distances_uplink_relay(set)


if __name__ == '__main__':
    main()
