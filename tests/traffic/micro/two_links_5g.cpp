/**
 * @file tests/two_links_5g.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/five-g/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include <artis-traffic/micro/five-g/Antenna.hpp>
#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Vehicle.hpp>

#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_TwoLinks_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>
#include <artis-traffic/micro/five-g/SignalProcessing.hpp>

using namespace std::chrono;
using namespace artis::traffic::micro::five_g;

/*************************************************
 * Tests
 *************************************************/

struct TwoLinksParameters {
    LinkParameters link1_parameters;
    LinkParameters link2_parameters;
    artis::traffic::micro::core::JunctionParameters junction_parameters;
    GeneratorParameters generator_parameters;
    AntennaParameters antenna_parameters;
    SignalProcessingParameters signal_processing1_parameters;
    SignalProcessingParameters signal_processing2_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksGraphManager :
        public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksParameters> {
public:
    enum sub_models {
        GENERATOR, LINK_1, JUNCTION, LINK_2, ANTENNA, SIGNAL_PROCESSING_1, SIGNAL_PROCESSING_2, END
    };

    TwoLinksGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                        const TwoLinksParameters &parameters,
                        const artis::common::NoParameters &graph_parameters)
            :
            MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, TwoLinksParameters>(
                    coordinator, parameters, graph_parameters),
            _generator("generator", parameters.generator_parameters),
            _link1("link1", parameters.link1_parameters),
            _link2("link2", parameters.link2_parameters),
            _junction("junction", parameters.junction_parameters),
            _antenna("antenna", parameters.antenna_parameters),
            _signal_processing_1("signal_processing1", parameters.signal_processing1_parameters),
            _signal_processing_2("signal_processing2", parameters.signal_processing2_parameters),
            _end("end", artis::common::NoParameters()) {
        this->add_child(GENERATOR, &_generator);
        this->add_child(LINK_1, &_link1);
        this->add_child(LINK_2, &_link2);
        this->add_child(JUNCTION, &_junction);
        this->add_child(ANTENNA, &_antenna);
        this->add_child(SIGNAL_PROCESSING_1, &_signal_processing_1);
        this->add_child(SIGNAL_PROCESSING_2, &_signal_processing_2);
        this->add_child(END, &_end);

        this->out({&_generator, artis::traffic::micro::five_g::Generator::outputs::OUT})
                >> this->in({&_link1,
                             artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::inputs::IN});
        this->connect_upstream_link_to_junction(_link1, _junction, 0);
        this->connect_downstream_link_to_junction(_link2, _junction, 0, 0);
        this->connect_link_to_end(_link2, _end);

        this->out({&_link1, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::outputs::ANTENNA_MESSAGE})
                >> this->in({&_signal_processing_1, SignalProcessing<SignalProcessingTypes<Vehicle,
                        VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN});
        this->out({&_signal_processing_1, SignalProcessing<SignalProcessingTypes<Vehicle,
                VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT})
                >> this->in({&_link1, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::inputs::ANTENNA_MESSAGE});

        this->out({&_antenna, Antenna::outputs::MESSAGE})
                >> this->in({&_signal_processing_1, SignalProcessing<SignalProcessingTypes<Vehicle,
                        VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN + 1});
        this->out({&_signal_processing_1, SignalProcessing<SignalProcessingTypes<Vehicle,
                VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT + 1})
                >> this->in({&_antenna, Antenna::inputs::MESSAGE});

        this->out({&_link2, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::outputs::ANTENNA_MESSAGE})
                >> this->in({&_signal_processing_2, SignalProcessing<SignalProcessingTypes<Vehicle,
                        VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN});
        this->out({&_signal_processing_2, SignalProcessing<SignalProcessingTypes<Vehicle,
                VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT})
                >> this->in({&_link2, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::inputs::ANTENNA_MESSAGE});

        this->out({&_antenna, Antenna::outputs::MESSAGE + 1})
                >> this->in({&_signal_processing_2, SignalProcessing<SignalProcessingTypes<Vehicle,
                        VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN + 1});
        this->out({&_signal_processing_2, SignalProcessing<SignalProcessingTypes<Vehicle,
                VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT + 1})
                >> this->in({&_antenna, Antenna::inputs::MESSAGE + 1});
    }

    ~TwoLinksGraphManager() override = default;

private:
    artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>,
            LinkParameters> _link1;
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>,
            LinkParameters> _link2;
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::core::Junction<Vehicle>, artis::traffic::micro::core::JunctionParameters> _junction;
    artis::pdevs::Simulator<artis::common::DoubleTime, Antenna, AntennaParameters> _antenna;
    artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing<SignalProcessingTypes<Vehicle,
            VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>,
            SignalProcessingParameters> _signal_processing_1;
    artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing<SignalProcessingTypes<Vehicle,
            VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>,
            SignalProcessingParameters> _signal_processing_2;
    artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class TwoLinksView : public artis::traffic::core::View {
public:
    TwoLinksView() {
        selector("Link1:vehicle_number",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_NUMBER});
        selector("Link1:vehicle_positions",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_POSITIONS});
        selector("Link1:vehicle_indexes",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_INDEXES});
        selector("Link1:vehicle_speed",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_1,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_SPEEDS});

        selector("Link2:vehicle_number",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_NUMBER});
        selector("Link2:vehicle_positions",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_POSITIONS});
        selector("Link2:vehicle_indexes",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_INDEXES});
        selector("Link2:vehicle_speed",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK_2,
                  Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_SPEEDS});
        
        selector("Antenna:vehicle_indexes",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
                  artis::traffic::micro::five_g::Antenna::vars::VEHICLE_INDEXES});
        selector("Antenna:vehicle_positions",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
                  artis::traffic::micro::five_g::Antenna::vars::VEHICLE_POSITIONS});
        selector("Antenna:measure_reports",
                 {TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
                  artis::traffic::micro::five_g::Antenna::vars::MEASURE_REPORTS});
    }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(TwoLinksParameters &parameters) {
    artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
    artis::common::RootCoordinator<
            artis::common::DoubleTime, artis::pdevs::Coordinator<
                    artis::common::DoubleTime,
                    TwoLinksGraphManager<Vehicle, VehicleEntry, VehicleState>,
                    TwoLinksParameters>
    > rc(context, "root", parameters, artis::common::NoParameters());

    rc.attachView("Links2", new TwoLinksView<Vehicle, VehicleEntry, VehicleState>());
    rc.switch_to_timed_observer(1);

    steady_clock::time_point t1 = steady_clock::now();

    rc.run(context);

    steady_clock::time_point t2 = steady_clock::now();

    duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

    std::cout << "Duration: " << time_span.count() << std::endl;

    artis::common::observer::Output<artis::common::DoubleTime,
            artis::common::observer::TimedIterator<artis::common::DoubleTime>>
            output(rc.observer());

    output(context.begin(), context.end(), {context.begin(), 1});
}

BOOST_AUTO_TEST_CASE(TestCase_TwoLinks_1)
{
    Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                        {artis::traffic::micro::five_g::VehicleData::State::RUNNING, 1,
                         0, Cell{"fake", 0, 0, 0, 0, 0}, {}}};

    Vehicle vehicle2 = {1, 4.5, 3, 4, 30, 1, 1, 1, {},
                        {artis::traffic::micro::five_g::VehicleData::State::RUNNING, 1,
                         1, Cell{"fake", 0, 0, 0, 0, 0}, {}}};

    std::vector<double> times = {0, 1};
    std::vector<Vehicle> vehicles = {vehicle1, vehicle2};

    // way/72140796_0_R
    std::vector<Coordinates > coordinates = {
            {
                    115725.21922641869,
                    3296227.7940304144
            },
            {
                    115738.42918370405,
                    3296238.32376403
            },
            {
                    115747.84739399086,
                    3296243.6459158114
            },
            {
                    115758.33307557344,
                    3296248.166225642
            }
    };
    // way/89475909_0
    std::vector<Coordinates > coordinates2 = {
            {
            115725.21922641869,
            3296227.7940304144
            },
            {
            115719.49268769649,
            3296230.5696479524
            },
            {
            115714.19424944186,
            3296236.98441566
            },
            {
            115703.84200177128,
            3296253.8937208774
            }
    };

    std::vector<Coordinates > antenna_coordinates = {
            {
                    115033.62570091385,
                    3295646.7003373937
            }
    };
    std::vector<Coordinates> signal_coordinates = {Coordinates{-1,-1},
                                                   antenna_coordinates[0]};
    TwoLinksParameters parameters = {{10,       71, 1, 0, 0, {}, coordinates},
                                     {10,       55, 1, 0, 0, {}, coordinates2},
                                     {1, 1},
                                        {vehicles, times},
                                     {antenna_coordinates, 2, {Cell{"1", 350, 290, 0, 0, 0}}},
                                     {signal_coordinates, {Cell{"1", 350, 290, 0, 0, 0}}},
                                     {signal_coordinates, {Cell{"1", 350, 290, 0, 0, 0}}}};
    run_simulation<Vehicle, EventGippsVehicleDynamics<Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
            parameters);

    BOOST_CHECK(true);
}