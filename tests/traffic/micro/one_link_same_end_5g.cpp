/**
 * @file tests/one_link_same_end.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/kernel/pdevs/Simulator.hpp>

#include <artis-traffic/micro/core/Link.hpp>
#include <artis-traffic/micro/utils/End.hpp>

#include <artis-traffic/micro/five-g/vehicle-dynamics/EventGippsVehicleDynamics.hpp>

#include <artis-traffic/micro/five-g/Antenna.hpp>
#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Vehicle.hpp>

#include "MicroGraphManager.hpp"

#define BOOST_TEST_MODULE Link_OneLinkSameEnd_Tests

#include <boost/test/unit_test.hpp>

#include <chrono>
#include <artis-traffic/micro/five-g/SignalProcessing.hpp>

using namespace std::chrono;
using namespace artis::traffic::micro::five_g;

/*************************************************
 * Tests
 *************************************************/

struct OnlyOneLinkParameters {
  LinkParameters link_parameters;
  GeneratorParameters generator_parameters;
  AntennaParameters antenna_parameters;
  SignalProcessingParameters signal_processing_parameters;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkGraphManager :
  public MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters> {
public:
  enum sub_models {
    GENERATOR, LINK, ANTENNA, SIGNAL_PROCESSING, END
  };

  OneLinkGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                      const OnlyOneLinkParameters &parameters,
                      const artis::common::NoParameters &graph_parameters)
    :
    MicroGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime, OnlyOneLinkParameters>(
      coordinator, parameters, graph_parameters),
    _generator("generator", parameters.generator_parameters),
    _link("link", parameters.link_parameters),
    _antenna("antenna", parameters.antenna_parameters),
    _signal_processing("signal_processing", parameters.signal_processing_parameters),
    _end("end", artis::common::NoParameters()) {
    this->add_child(GENERATOR, &_generator);
    this->add_child(LINK, &_link);
    this->add_child(ANTENNA, &_antenna);
    this->add_child(SIGNAL_PROCESSING, &_signal_processing);
    this->add_child(END, &_end);

    this->out({&_generator, artis::traffic::micro::five_g::Generator::outputs::OUT})
      >> this->in({&_link,
                   artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::inputs::IN});
    this->connect_link_to_end(_link, _end);

      this->out({&_link, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::outputs::ANTENNA_MESSAGE})
              >> this->in({&_signal_processing, SignalProcessing<SignalProcessingTypes<Vehicle,
                      VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN});
      this->out({&_signal_processing, SignalProcessing<SignalProcessingTypes<Vehicle,
              VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT})
              >> this->in({&_link, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::inputs::ANTENNA_MESSAGE});

      this->out({&_antenna, Antenna::outputs::MESSAGE})
              >> this->in({&_signal_processing, SignalProcessing<SignalProcessingTypes<Vehicle,
                      VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::inputs::IN + 1});
      this->out({&_signal_processing, SignalProcessing<SignalProcessingTypes<Vehicle,
              VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>::outputs::OUT + 1})
              >> this->in({&_antenna, Antenna::inputs::MESSAGE});
//    this->out({&_link,
//               artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::outputs::ANTENNA_MESSAGE})
//      >> this->in({&_antenna, artis::traffic::micro::five_g::Antenna::inputs::MESSAGE});
  }

  ~OneLinkGraphManager() override = default;

private:
  artis::pdevs::Simulator<artis::common::DoubleTime, Generator, GeneratorParameters> _generator;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>,
    LinkParameters> _link;
  artis::pdevs::Simulator<artis::common::DoubleTime, Antenna, AntennaParameters> _antenna;
    artis::pdevs::Simulator<artis::common::DoubleTime, SignalProcessing<SignalProcessingTypes<Vehicle,
    VehicleEntry, VehicleState, SignalProcessingParameters>, SignalProcessingParameters>,
    SignalProcessingParameters> _signal_processing;
  artis::pdevs::Simulator<artis::common::DoubleTime, artis::traffic::micro::utils::End> _end;
};

template<class Vehicle, class VehicleEntry, class VehicleState>
class OneLinkView : public artis::traffic::core::View {
public:
  OneLinkView() {
    selector("Link:vehicle_number",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_NUMBER});
    selector("Link:vehicle_positions",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_POSITIONS});
    selector("Link:vehicle_indexes",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_INDEXES});
    selector("Link:vehicle_speed",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_SPEEDS});
    selector("Link:vehicle_states",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::LINK,
              Link<LinkTypes<Vehicle, VehicleEntry, VehicleState, LinkParameters>, LinkParameters>::vars::VEHICLE_STATES});
    selector("Antenna:vehicle_indexes",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
              artis::traffic::micro::five_g::Antenna::vars::VEHICLE_INDEXES});
    selector("Antenna:vehicle_positions",
             {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
              artis::traffic::micro::five_g::Antenna::vars::VEHICLE_POSITIONS});
      selector("Antenna:measure_reports",
               {OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>::ANTENNA,
                artis::traffic::micro::five_g::Antenna::vars::MEASURE_REPORTS});
  }
};

template<class Vehicle, class VehicleEntry, class VehicleState>
void run_simulation(OnlyOneLinkParameters &parameters) {
  artis::common::context::Context<artis::common::DoubleTime> context(0, 3600);
  artis::common::RootCoordinator<
    artis::common::DoubleTime, artis::pdevs::Coordinator<
      artis::common::DoubleTime,
      OneLinkGraphManager<Vehicle, VehicleEntry, VehicleState>,
      OnlyOneLinkParameters>
  > rc(context, "root", parameters, artis::common::NoParameters());

  rc.attachView("Link", new OneLinkView<Vehicle, VehicleEntry, VehicleState>());
  rc.switch_to_timed_observer(1);

  steady_clock::time_point t1 = steady_clock::now();

  rc.run(context);

  steady_clock::time_point t2 = steady_clock::now();

  duration<double> time_span = duration_cast<duration<double> >(t2 - t1);

  std::cout << "Duration: " << time_span.count() << std::endl;

  artis::common::observer::Output<artis::common::DoubleTime,
    artis::common::observer::TimedIterator<artis::common::DoubleTime>>
    output(rc.observer());

  output(context.begin(), context.end(), {context.begin(), 1});
}

BOOST_AUTO_TEST_CASE(TestCase_SameEnd__1)
{
  Vehicle vehicle1 = {0, 4.5, 3, 10, 10, 1, 1, 1, {},
                      {artis::traffic::micro::five_g::VehicleData::State::RUNNING, 1,
                       0, Cell{"fake", 0, 0, 0, 0, 0}, {}}};

  Vehicle vehicle2 = {1, 4.5, 3, 4, 30, 1, 1, 1, {},
                      {artis::traffic::micro::five_g::VehicleData::State::RUNNING, 1,
                       1, Cell{"fake", 0, 0, 0, 0, 0}, {}}};

  std::vector<double> times = {0, 1};
  std::vector<Vehicle> vehicles = {vehicle1, vehicle2};

  std::vector<Coordinates > coordinates = {
          {
                  115725.21922641869,
                  3296227.7940304144
          },
          {
                  115738.42918370405,
                  3296238.32376403
          },
          {
                  115747.84739399086,
                  3296243.6459158114
          },
          {
                  115758.33307557344,
                  3296248.166225642
          }
  };

  std::vector<Coordinates > antenna_coordinates = {
          {
          115033.62570091385,
          3295646.7003373937
          }
  };
    std::vector<Coordinates> signal_coordinates = {Coordinates{-1,-1},
                                            antenna_coordinates[0]};
  OnlyOneLinkParameters parameters = {{10,       71, 1, 0, 0, {}, coordinates},
                                      {vehicles, times},
                                      {antenna_coordinates, 1, {Cell{"1", 350, 290, 0, 0, 0}}},
                                      {signal_coordinates, {Cell{"1", 350, 290, 0, 0, 0}}}};
  run_simulation<Vehicle, EventGippsVehicleDynamics<Vehicle>, artis::traffic::micro::core::EventGippsVehicleState>(
    parameters);

  BOOST_CHECK(true);
}