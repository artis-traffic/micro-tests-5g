/**
 * @file traffic/five_g/JSONNetworkGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_MICRO_TESTS_5G_JSON_NETWORK_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_MICRO_TESTS_5G_JSON_NETWORK_GRAPH_MANAGER_HPP

#include <artis-traffic/micro/utils/Generator.hpp>
#include <artis-traffic/micro/five-g/JSONNetworkGraphManager.hpp>
#include <artis-traffic/micro/utils/JSONNetworkView.hpp>

#include <artis-traffic/micro/core/link-state-machine/LinkTypes.hpp>
#include <artis-traffic/micro/core/stop-state-machine/StopTypes.hpp>

#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Stop.hpp>

#include <artis-traffic/micro/five-g/VehicleFactory.hpp>

template<class Vehicle, class VehicleEntry, class VehicleState,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONNetworkGraphManager = artis::traffic::micro::five_g::JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::five_g::StopParameters,
  artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
  artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
  Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONLinkView = artis::traffic::micro::five_g::JSONLinkView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::core::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::five_g::StopParameters,
  artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
        artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
                artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONStopView = artis::traffic::micro::five_g::JSONStopView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::five_g::StopParameters,
  artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
        artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
                artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONJunctionView = artis::traffic::micro::utils::JSONJunctionView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::five_g::StopParameters,
  artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
  typename Time, typename Parameters = artis::common::NoParameters>
using JSONGeneratorView = artis::traffic::micro::utils::JSONGeneratorView<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::five_g::StopParameters,
  artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
  artis::traffic::micro::utils::GeneratorParameters,
  Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
        typename Time, typename Parameters = artis::common::NoParameters>
using JSONAntennaView = artis::traffic::micro::five_g::JSONAntennaView<Vehicle, VehicleEntry, VehicleState,
        artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
                artis::traffic::micro::five_g::LinkParameters>,
        artis::traffic::micro::five_g::LinkParameters,
        artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
                artis::traffic::micro::five_g::StopParameters>,
        artis::traffic::micro::five_g::StopParameters,
        artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
        artis::traffic::micro::utils::GeneratorParameters,
        artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
                artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
        Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
        typename Time, typename Parameters = artis::common::NoParameters>
using JSONRelayView = artis::traffic::micro::five_g::JSONRelayView<Vehicle, VehicleEntry, VehicleState,
        artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
                artis::traffic::micro::five_g::LinkParameters>,
        artis::traffic::micro::five_g::LinkParameters,
        artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
                artis::traffic::micro::five_g::StopParameters>,
        artis::traffic::micro::five_g::StopParameters,
        artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
        artis::traffic::micro::utils::GeneratorParameters,
        artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
                artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
        Network, Time, Parameters>;

template<class Vehicle, class VehicleEntry, class VehicleState, class Network,
        typename Time, typename Parameters = artis::common::NoParameters>
using JSONSignalProcessingView = artis::traffic::micro::five_g::JSONSignalProcessingView<Vehicle, VehicleEntry, VehicleState,
        artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
                artis::traffic::micro::five_g::LinkParameters>,
        artis::traffic::micro::five_g::LinkParameters,
        artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
                artis::traffic::micro::five_g::StopParameters>,
        artis::traffic::micro::five_g::StopParameters,
        artis::traffic::micro::utils::Generator<artis::traffic::micro::five_g::VehicleFactory>,
        artis::traffic::micro::utils::GeneratorParameters,
        artis::traffic::micro::five_g::SignalProcessing<artis::traffic::micro::five_g::SignalProcessingTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::SignalProcessingParameters>,
                artis::traffic::micro::five_g::SignalProcessingParameters>,
        artis::traffic::micro::five_g::SignalProcessingParameters,
        artis::traffic::micro::five_g::Antenna<artis::traffic::micro::five_g::AntennaTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::AntennaParameters>,
                artis::traffic::micro::five_g::AntennaParameters>,
        artis::traffic::micro::five_g::AntennaParameters,
        artis::traffic::micro::five_g::Relay<artis::traffic::micro::five_g::RelayTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::RelayParameters>,
                artis::traffic::micro::five_g::RelayParameters>,
        artis::traffic::micro::five_g::RelayParameters,
        Network, Time, Parameters>;
#endif //ARTIS_TRAFFIC_MICRO_TESTS_5G_JSON_NETWORK_GRAPH_MANAGER_HPP
