/**
* @file tests-fiveg/json_network.cpp
* @author The ARTIS Development Team
    * See the AUTHORS or Authors.txt file
*/

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <artis-traffic/micro/five-g/JsonReader.hpp>

#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/common/observer/Output.hpp>
#include <artis-star/common/observer/TimedIterator.hpp>

#include <artis-traffic/micro/five-g/Vehicle.hpp>

#include <artis-traffic/micro/core/vehicle-dynamics/EventGippsVehicleDynamics.hpp>
//#include <artis-traffic/micro/core/vehicle-dynamics/TimeGippsVehicleDynamics.hpp>

#include "JSONNetworkGraphManager.hpp"

#include <iostream>

template<class Vehicle, class VehicleEntry, class VehicleState>
class Runner {
public:
  Runner(const std::string &path) : _path(path) {}

  void operator()(unsigned int index) {
    std::ifstream input(_path + "test_audruicq_near_antenna_norelay_set" + std::to_string(index) + ".json");
    std::string comm_type = "antenna";

    if (input) {
      std::string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

      artis::traffic::micro::five_g::JsonReader reader;

      reader.parse_network(str);

      const auto &network = reader.network();

      artis::common::context::Context<artis::common::DoubleTime> context(0, 1500);
      artis::common::RootCoordinator<
        artis::common::DoubleTime, artis::pdevs::Coordinator<
          artis::common::DoubleTime,
          JSONNetworkGraphManager<Vehicle, VehicleEntry, VehicleState, artis::common::DoubleTime>,
          artis::common::NoParameters,
          artis::traffic::micro::five_g::Network>
      > rc(context, "root", artis::common::NoParameters(), network);

      rc.attachView("Link_event_"+ comm_type + "_set" + std::to_string(index),
                    new JSONLinkView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(
                      network));
      rc.attachView("Stop_event_"+ comm_type + "_set" + std::to_string(index), new JSONStopView<Vehicle, VehicleEntry, VehicleState,
                    artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(network));
//      rc.attachView("Generator_event_set" + std::to_string(index),
//                    new JSONGeneratorView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(
//                      network));
//      rc.attachView("Junction_time_set" + std::to_string(index), new JSONJunctionView<Vehicle, VehicleEntry, VehicleState>(network));
        rc.attachView("Antenna_event_"+ comm_type + "_set" + std::to_string(index),
                      new JSONAntennaView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(
                              network));
        rc.attachView("Relay_event_"+ comm_type + "_set" + std::to_string(index),
                      new JSONRelayView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(
                              network));
        rc.attachView("Signal_event_"+ comm_type + "_set" + std::to_string(index),
                      new JSONSignalProcessingView<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::Network, artis::common::DoubleTime>(
                              network));
      rc.switch_to_timed_observer(0.1);

      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();

      rc.run(context);

//        const double window = 60;
//
//        rc.start(context);
//        while (rc.tn() < context.end()) {
//            double d = context.end() - rc.tn() < window ? context.end() - rc.tn() : window;
//
//            rc.run(d);
//        }

      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
      std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1);

      std::cout << "Set " + std::to_string(index) << std::endl;
      std::cout << "Duration: " << time_span.count() << std::endl;

      artis::common::observer::Output<artis::common::DoubleTime,
        artis::common::observer::TimedIterator<artis::common::DoubleTime>>
        output(rc.observer());

      output(context.begin(), context.end(), {context.begin(), 0.1});
    }
  }

private:
  std::string _path;
};

int main() {
  Runner<artis::traffic::micro::five_g::Vehicle,
    artis::traffic::micro::core::EventGippsVehicleDynamics<artis::traffic::micro::five_g::Vehicle>,
    artis::traffic::micro::core::EventGippsVehicleState> runner("../../../../scripts/");

  for (int i = 6; i < 7; i++) {
    runner(i);
  }
  return 0;
}

