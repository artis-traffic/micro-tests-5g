/**
 * @file tests/MicroGraphManager.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2023 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_TRAFFIC_TESTS_MICRO_GRAPH_MANAGER_HPP
#define ARTIS_TRAFFIC_TESTS_MICRO_GRAPH_MANAGER_HPP

#include <artis-traffic/micro/five-g/Generator.hpp>
#include <artis-traffic/micro/five-g/Link.hpp>
#include <artis-traffic/micro/five-g/Stop.hpp>

#include <artis-traffic/micro/utils/MicroGraphManager.hpp>

template<class Vehicle, class VehicleEntry, class VehicleState,
  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
using MicroGraphManager = artis::traffic::micro::utils::MicroGraphManager<Vehicle, VehicleEntry, VehicleState,
  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::LinkTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::LinkParameters>,
    artis::traffic::micro::five_g::LinkParameters>,
  artis::traffic::micro::five_g::LinkParameters,
  artis::traffic::micro::five_g::Stop<artis::traffic::micro::five_g::StopTypes<Vehicle, VehicleEntry, VehicleState, artis::traffic::micro::five_g::StopParameters>,
    artis::traffic::micro::five_g::StopParameters>,
  artis::traffic::micro::core::StopParameters,
  artis::traffic::micro::five_g::Generator,
  artis::traffic::micro::five_g::GeneratorParameters,
  Time, Parameters, GraphParameters>;

//template<class Vehicle, class VehicleEntry, class VehicleState,
//  typename Time, typename Parameters = artis::common::NoParameters, typename GraphParameters = artis::common::NoParameters>
//using MicroGraphManager = artis::traffic::micro::utils::MicroGraphManager<Vehicle, VehicleEntry, VehicleState,
//  artis::traffic::micro::five_g::Link<artis::traffic::micro::five_g::Types<artis::traffic::micro::five_g::Vehicle, VehicleEntry, VehicleState>>,
//  artis::traffic::micro::five_g::LinkParameters,
//  artis::traffic::micro::five_g::Generator,
//  artis::traffic::micro::five_g::GeneratorParameters,
//  Time, Parameters, GraphParameters>;

#endif //ARTIS_TRAFFIC_TESTS_MICRO_GRAPH_MANAGER_HPP
